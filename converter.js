const CryptoConvert = require("crypto-convert").default;
const IP = require("ip");

const upload_error = require("../../node_error_functions/upload_error");
const convert = new CryptoConvert({
  cryptoInterval: 5000, //Crypto prices update interval in ms (default 5 seconds on Node.js & 15 seconds on Browsers)
  fiatInterval: 60 * 1e3 * 60, //Fiat prices update interval (default every 1 hour)
  calculateAverage: true, //Calculate the average crypto price from exchanges
  binance: true, //Use binance rates
  bitfinex: true, //Use bitfinex rates
  coinbase: true, //Use coinbase rates
  kraken: true, //Use kraken rates
  HTTPAgent: null, //HTTP Agent for server-side proxies (Node.js only)
});

module.exports.currencyConverter = async (from, to, amount) => {
  // if no conversion is needed.
  try {
    await convert.ready();
    const conversion = convert[from][to](amount);
    return conversion;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Converting Currency",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
